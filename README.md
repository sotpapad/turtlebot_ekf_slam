# Turtlebot_EKF_SLAM

Attempt to create an EKF based SLAM using the turtlebot in ROS (python 3) with Gazebo and Rviz.

Currently some hyperparameters adjustments need to be made, in order for the
estimated robot state to be more accurate.

The landmarks estimated position and uncertainty are, for the time being, updated
serially so only the most recently found is updated.
